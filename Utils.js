import XLSC from 'xlsx';

let Utils;

Utils.areMetaTagsIdentical = function (elem1, elem2) {
    // find elem with shorter meta tag array
    let shorter;
    let longer;
    if (elem1.meta_tags.length <= elem2.meta_tags.length) {
        shorter = elem1.meta_tags;
        longer = elem2.meta_tags;
    } else {
        shorter = elem2.meta_tags;
        longer = elem1.meta_tags;
    }
    for (let i = 0; i < shorter.length; i++) {
        if (longer.indexOf(shorter[i]) < 0) {
            return false;
        }
    }
    return true;
}

Utils.deleteDoubles = function (entries) {
    let identicalFound;
    do {
        identicalFound = false;
        let i = 0;
        while (i < entries.length) {
            let currEntry = entries[i]; 
            let j = entries.length - 1;
            while (j > i) {
                let otherEntry = entries[j];
                if (currEntry.target_expression === otherEntry.target_expression 
                    && currEntry.source_expression === otherEntry.source_expression) {
                    console.log('deleting identical entry!');
                    entries.splice(j, 1);
                    identicalFound = true;
                }
                --j;
            }
            ++i;
        }
    } while (identicalFound === true);
    return entries;
}

Utils.combineIdenticalTranslations = function (entries) {
    let doubleFound;
    do {
        doubleFound = false;
        let i = 0;
        while (i < entries.length) {
            let currEntry = entries[i]; 
            let j = entries.length - 1;
            while (j > i) {
                let otherEntry = entries[j];
                // compare if identical
                if (currEntry.target_expression === otherEntry.target_expression) {
                    // compare meta-tags
                    if (areMetaTagsIdentical(currEntry, otherEntry)) {
                        console.log('combining identical translation!');
                        currEntry.source_expression += ', ' + otherEntry.source_expression;
                        entries.splice(j, 1)
                        doubleFound = true;
                    }
                }
                --j;
            }
            ++i;
        }
    } while (doubleFound === true);
    return entries;
}

Utils.exportExcel = function (data) {
    let out = [];
    for (let i = 0; i < data.length; i++) {
        const element = data[i];
        let row = [];
        // row.push(element.)
        // use aoa_to_sheet
    }
}

export default Utils;
