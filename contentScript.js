// import Utils from './Utils';

console.log('content-script running!');

let lessonSelected = false;
let currentList;

document.addEventListener('click', function (e) {
    if (e.currentTarget.activeElement && e.currentTarget.activeElement.className === 'lesson__options') {
        console.log('click occured!');
        lessonSelected = true;
        // myPort.postMessage('export_excel');
    }
});

function onNewListSelected (newList) {
    currentList = newList;

    const listElement = document.createElement('div');
    const textContent = document.createTextNode('Export improved list (as .xlsx)');

    listElement.appendChild(textContent);
    listElement.addEventListener('click', exportList);

    const dropDown = document.querySelector('.lesson-navbar__quick-links>li:nth-child(4) ul');
    if (dropDown) {
        dropDown.appendChild(listElement);
        console.log('dropdown appended!');
    }
}

function exportList () {
    myPort.postMessage('export_excel');
}

console.log('adding listener');
var myPort = browser.runtime.connect({name:"port-from-cs"});
myPort.postMessage({greeting: "hello from content script"});

myPort.onMessage.addListener((newList, sender, sendResponse) => {
    onNewListSelected(newList);
});