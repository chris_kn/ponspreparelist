// import Utils from './Utils';
// import XLSC from 'xlsx';

let current_list;
let Utils = {};
let myPort;

Utils.areMetaTagsIdentical = function (elem1, elem2) {
    // find elem with shorter meta tag array
    let shorter;
    let longer;
    if (elem1.meta_tags.length <= elem2.meta_tags.length) {
        shorter = elem1.meta_tags;
        longer = elem2.meta_tags;
    } else {
        shorter = elem2.meta_tags;
        longer = elem1.meta_tags;
    }
    for (let i = 0; i < shorter.length; i++) {
        if (longer.indexOf(shorter[i]) < 0) {
            return false;
        }
    }
    return true;
}

Utils.deleteDoubles = function (entries) {
    let identicalFound;
    do {
        identicalFound = false;
        let i = 0;
        while (i < entries.length) {
            let currEntry = entries[i]; 
            let j = entries.length - 1;
            while (j > i) {
                let otherEntry = entries[j];
                if (currEntry.target_expression === otherEntry.target_expression 
                    && currEntry.source_expression === otherEntry.source_expression) {
                    console.log('deleting identical entry!');
                    entries.splice(j, 1);
                    identicalFound = true;
                }
                --j;
            }
            ++i;
        }
    } while (identicalFound === true);
    return entries;
}

Utils.combineIdenticalTranslations = function (entries) {
    let doubleFound;
    do {
        doubleFound = false;
        let i = 0;
        while (i < entries.length) {
            let currEntry = entries[i]; 
            let j = entries.length - 1;
            while (j > i) {
                let otherEntry = entries[j];
                // compare if identical
                if (currEntry.target_expression === otherEntry.target_expression) {
                    // compare meta-tags
                    if (Utils.areMetaTagsIdentical(currEntry, otherEntry)) {
                        console.log('combining identical translation!');
                        currEntry.source_expression += ', ' + otherEntry.source_expression;
                        entries.splice(j, 1)
                        doubleFound = true;
                    }
                }
                --j;
            }
            ++i;
        }
    } while (doubleFound === true);
    return entries;
}

Utils.exportExcel = function (data) {
    let out = [[1,2,3],[1,2,3],[1,2,3]];
    // for (let i = 0; i < data.length; i++) {
    //     const element = data[i];
    //     let row = [];
    //     // row.push(element.)
    //     // use aoa_to_sheet
    // }
    let ws = XLSX.utils.aoa_to_sheet(out);
    let wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");
    XLSX.writeFile(wb, 'out.xlsb');
}

function listener (details) {
    let filter = browser.webRequest.filterResponseData(details.requestId);
    let decoder = new TextDecoder("utf-8");
    let encoder = new TextEncoder();

    let str = "";
    filter.ondata = event => {
        str += decoder.decode(event.data, {stream: true});
    };

    filter.onstop = event => {
        let jsonData = JSON.parse(str);

        if (jsonData.lesson && jsonData.lesson.entries) {
            let entries = jsonData.lesson.entries;

            // delete identical entries (source & target exp. are the same)
            entries = Utils.deleteDoubles(entries);

            // check if target expressions are identical
            entries = Utils.combineIdenticalTranslations(entries);

            // share new entries list with content-script
            console.log('sending message');
            // browser.runtime.sendMessage(entries);
            myPort.postMessage(entries);

            current_list = entries;
        }

        filter.write(encoder.encode(str));
        filter.close();
    };
}

function onContentScriptMessage (message) {
    console.log(message);
    if (message === 'export_excel') {
        Utils.exportExcel(current_list);
    }
}

browser.runtime.onConnect.addListener((p) => {
    myPort = p;
    myPort.onMessage.addListener(onContentScriptMessage);
});
browser.webRequest.onBeforeRequest.addListener(listener, { urls: ["https://trainer.pons.com/lessons/*"] }, ["blocking"]);

